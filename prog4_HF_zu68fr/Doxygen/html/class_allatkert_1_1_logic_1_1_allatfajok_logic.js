var class_allatkert_1_1_logic_1_1_allatfajok_logic =
[
    [ "AllatfajokLogic", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#a64e6d01e04c9bf66410e61f08a92f8bf", null ],
    [ "AllatfajokLogic", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#a7ef33fef3c4102ff2371827d16fe5bf8", null ],
    [ "DeleteAllatfajok", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#a896192ae8abe854025db986fc066aa88", null ],
    [ "GetAllAllatfajok", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#abb1b2cc8d3dc46458c3248c9fa9432c1", null ],
    [ "GetOneAllatfajok", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#aab1c6ba385a4f682a49529dce0d59219", null ],
    [ "InsertAllatfajok", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#a4b3721a307969eedeeb24d0caf1f9874", null ],
    [ "UpdateAllatfajok", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#a4bb988378f2df3e6b794e8d22f0065c9", null ],
    [ "GetRepo", "class_allatkert_1_1_logic_1_1_allatfajok_logic.html#a71e562f6364b29b5aa51f852e0cac543", null ]
];