var interface_allatkert_1_1_logic_1_1_i_allatok_logic =
[
    [ "AllatEsTaplalkozas", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#af01bc3fec57f72d08e163857b1907fe9", null ],
    [ "AllatfajosDarab", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#a12f83e182c0dab8174212c7091d7ad67", null ],
    [ "DeleteAllatok", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#a00d4dea3d0d3d0895a70ce1575da0675", null ],
    [ "GetAllAllatok", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#a12c1c3542941ab9799984612331c9560", null ],
    [ "GetOneAllatok", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#a5d2fe8d395058b66eaeecaf56c12525c", null ],
    [ "InsertAllatok", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#a2da1a3a355a704a877433a379aef601b", null ],
    [ "UpdateAllatok", "interface_allatkert_1_1_logic_1_1_i_allatok_logic.html#a848e0085783ee67079dbddf2e467c367", null ]
];