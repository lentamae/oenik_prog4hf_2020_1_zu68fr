var searchData=
[
  ['iallatfajoklogic_105',['IAllatfajokLogic',['../interface_allatkert_1_1_logic_1_1_i_allatfajok_logic.html',1,'Allatkert::Logic']]],
  ['iallatfajokrepository_106',['IAllatfajokRepository',['../interface_allatkert_1_1_repository_1_1_i_allatfajok_repository.html',1,'Allatkert::Repository']]],
  ['iallatoklogic_107',['IAllatokLogic',['../interface_allatkert_1_1_logic_1_1_i_allatok_logic.html',1,'Allatkert::Logic']]],
  ['iallatokrepository_108',['IAllatokRepository',['../interface_allatkert_1_1_repository_1_1_i_allatok_repository.html',1,'Allatkert::Repository']]],
  ['igondozoklogic_109',['IGondozokLogic',['../interface_allatkert_1_1_logic_1_1_i_gondozok_logic.html',1,'Allatkert::Logic']]],
  ['igondozokrepository_110',['IGondozokRepository',['../interface_allatkert_1_1_repository_1_1_i_gondozok_repository.html',1,'Allatkert::Repository']]],
  ['ikifutoklogic_111',['IKifutokLogic',['../interface_allatkert_1_1_logic_1_1_i_kifutok_logic.html',1,'Allatkert::Logic']]],
  ['ikifutokrepository_112',['IKifutokRepository',['../interface_allatkert_1_1_repository_1_1_i_kifutok_repository.html',1,'Allatkert::Repository']]],
  ['irepository_113',['IRepository',['../interface_allatkert_1_1_repository_1_1_i_repository.html',1,'Allatkert::Repository']]],
  ['irepository_3c_20allatfajok_20_3e_114',['IRepository&lt; Allatfajok &gt;',['../interface_allatkert_1_1_repository_1_1_i_repository.html',1,'Allatkert::Repository']]],
  ['irepository_3c_20allatok_20_3e_115',['IRepository&lt; Allatok &gt;',['../interface_allatkert_1_1_repository_1_1_i_repository.html',1,'Allatkert::Repository']]],
  ['irepository_3c_20gondozok_20_3e_116',['IRepository&lt; Gondozok &gt;',['../interface_allatkert_1_1_repository_1_1_i_repository.html',1,'Allatkert::Repository']]],
  ['irepository_3c_20kifutok_20_3e_117',['IRepository&lt; Kifutok &gt;',['../interface_allatkert_1_1_repository_1_1_i_repository.html',1,'Allatkert::Repository']]]
];
