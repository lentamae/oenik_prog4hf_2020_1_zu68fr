﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Allatkert.Data;
    using Allatkert.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// test class.
    /// </summary>
    [TestFixture]
    internal class LogicTest
    {
        /// <summary>
        /// testing the getall method.
        /// </summary>
        [Test]
        public void GetAllAllatTest()
        {
            Mock<IAllatokRepository> mockedRepo = new Mock<IAllatokRepository>(MockBehavior.Loose);
            List<Allatok> allatok = new List<Allatok>()
            {
                new Allatok() { AllatID = 1, AllatNev = "Mici", FajID = 1, KifutoID = 1, Nem = "Hím", SzuletesiIdo = Convert.ToDateTime("2018.10.03") },
                new Allatok() { AllatID = 2, AllatNev = "asdsadsads", FajID = 1, KifutoID = 1, Nem = "Hím", SzuletesiIdo = Convert.ToDateTime("2018.10.03") },
            };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(allatok.AsQueryable());
            AllatokLogic logic = new AllatokLogic(mockedRepo.Object);

            var result = logic.GetAllAllatok();

            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
            Assert.That(result.Select(x => x.AllatNev), Does.Contain("asdsadsads"));
        }

        /// <summary>
        /// testing the getone method.
        /// </summary>
        [Test]
        public void GetOneAllatTest()
        {
            Mock<IAllatokRepository> mockedRepo = new Mock<IAllatokRepository>(MockBehavior.Loose);
            List<Allatok> allatok = new List<Allatok>()
            {
                new Allatok() { AllatID = 1, AllatNev = "Mici", FajID = 1, KifutoID = 1, Nem = "Hím", SzuletesiIdo = Convert.ToDateTime("2018.10.03") },
                new Allatok() { AllatID = 2, AllatNev = "asdsadsads", FajID = 1, KifutoID = 1, Nem = "Hím", SzuletesiIdo = Convert.ToDateTime("2018.10.03") },
            };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(allatok.AsQueryable());
            mockedRepo.Setup(m => m.GetOne(It.IsAny<int>())).Returns((int i) => allatok.Where(t => t.AllatID == i).Single());
            AllatokLogic logic = new AllatokLogic(mockedRepo.Object);

            var egy = logic.GetOneAllatok("2");

            Assert.That(egy.AllatNev == "asdsadsads");
        }

        /// <summary>
        /// testing the update method.
        /// </summary>
        [Test]
        public void UpdateAllatokTest()
        {
            Mock<IAllatokRepository> mockedRepo = new Mock<IAllatokRepository>(MockBehavior.Loose);
            List<Allatok> allatok = new List<Allatok>()
            {
                new Allatok() { AllatID = 1, AllatNev = "Mici", FajID = 1,  KifutoID = 1, Nem = "Hím", SzuletesiIdo = Convert.ToDateTime("2018.10.03") },
                new Allatok() { AllatID = 2, AllatNev = "asdsadsads", FajID = 1, KifutoID = 1, Nem = "Hím", SzuletesiIdo = Convert.ToDateTime("2018.10.03") },
            };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(allatok.AsQueryable());
            mockedRepo.Setup(m => m.GetOne(It.IsAny<int>())).Returns((int i) => allatok.Where(t => t.AllatID == i).Single());
            AllatokLogic logic = new AllatokLogic(mockedRepo.Object);

            logic.UpdateAllatok("2", "Vasember");
            var result = logic.GetOneAllatok("2");

            mockedRepo.Verify(repo => repo.UpdateAllatok(2, "Vasember"), Times.Once);
        }

        /// <summary>
        /// testing the delete method.
        /// </summary>
        [Test]
        public void DeleteGondozokTest()
        {
            Mock<IGondozokRepository> mockedRepo = new Mock<IGondozokRepository>(MockBehavior.Loose);
            List<Gondozok> gondozok = new List<Gondozok>()
            {
                new Gondozok() { GondozoID = 1, GondozoNev = "Pista", MunkaviszKezd = Convert.ToDateTime("1998.06.20"), Telefonszam = "06209988456" },
                new Gondozok() { GondozoID = 2, GondozoNev = "Józsi", MunkaviszKezd = Convert.ToDateTime("2000.10.02"), Telefonszam = "06706894426" },
                new Gondozok() { GondozoID = 3, GondozoNev = "Géza", MunkaviszKezd = Convert.ToDateTime("1998.01.01"), Telefonszam = "06301234455" },
            };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(gondozok.AsQueryable());
            mockedRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns((int i) => gondozok.Where(t => t.GondozoID == i).Single());
            mockedRepo.Setup(repo => repo.Delete(It.IsAny<int>()));

            GondozokLogic logic = new GondozokLogic(mockedRepo.Object);
            logic.DeleteGondozok("2");

            mockedRepo.Verify(repo => repo.Delete(2));
        }

        /// <summary>
        /// testing the insert method.
        /// </summary>
        [Test]
        public void InsertAllatfajokTest()
        {
            Mock<IAllatfajokRepository> mockedRepo = new Mock<IAllatfajokRepository>(MockBehavior.Loose);
            var itemsInserted = new List<Allatfajok>();
            mockedRepo.Setup(x => x.InsertAllatfajok(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()));
            AllatfajokLogic logic = new AllatfajokLogic(mockedRepo.Object);

            logic.InsertAllatfajok("1", "Narvál", "Ragadozó");

            mockedRepo.Verify(repo => repo.InsertAllatfajok(1, "Narvál", "Ragadozó"), Times.Once);
        }

        /// <summary>
        /// testing a nonCRUD method no1.
        /// </summary>
        [Test]
        public void KifutoGondozoCountTest()
        {
            Mock<IKifutokRepository> mockedRepo = new Mock<IKifutokRepository>(MockBehavior.Loose);
            List<Kifutok> kifutok = new List<Kifutok>()
            {
                new Kifutok() { GondozoID = 1, KifutoID = 1, KifutoMeret = "30x30", KifutoNev = "Kif1" },
                new Kifutok() { GondozoID = 1, KifutoID = 2, KifutoMeret = "30x30", KifutoNev = "Kif2" },
                new Kifutok() { GondozoID = 2, KifutoID = 3, KifutoMeret = "30x30", KifutoNev = "Kif3" },
            };

            List<GondozoKifutoCountResult> expected = new List<GondozoKifutoCountResult>()
            {
                new GondozoKifutoCountResult() { GondozoId = "1", KifutoDb = 2 },
                new GondozoKifutoCountResult() { GondozoId = "2", KifutoDb = 1 },
            };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(kifutok.AsQueryable());
            KifutokLogic logic = new KifutokLogic(mockedRepo.Object);

            var result = logic.GondozoKifutoCount();

            mockedRepo.Verify(repo => repo.GondozoKifutoCount(), Times.Exactly(1));
        }

        /// <summary>
        /// testing a nonCRUD method no2.
        /// </summary>
        [Test]
        public void AllatEsTaplalkozas()
        {
            Mock<IAllatfajokRepository> fajMockedRepo = new Mock<IAllatfajokRepository>(MockBehavior.Loose);
            Mock<IAllatokRepository> allatMockedRepo = new Mock<IAllatokRepository>(MockBehavior.Loose);
            List<Allatfajok> fajok = new List<Allatfajok>()
            {
                new Allatfajok() { FajID = 1, FajNev = "cica", Taplalkozas = "Növényevő" },
                new Allatfajok() { FajID = 2, FajNev = "kutya", Taplalkozas = "Ragadozó" },
            };
            List<Allatok> allatok = new List<Allatok>()
            {
                new Allatok() { AllatNev = "Csilla", FajID = 1, AllatID = 1, },
                new Allatok() { AllatNev = "Cinike", FajID = 1, AllatID = 2, },
                new Allatok() { AllatNev = "DOG", FajID = 2, AllatID = 3, },
            };
            fajMockedRepo.Setup(repo => repo.GetAll()).Returns(fajok.AsQueryable());
            allatMockedRepo.Setup(repo => repo.GetAll()).Returns(allatok.AsQueryable());
            AllatokLogic logic = new AllatokLogic(allatMockedRepo.Object);
            AllatfajokLogic seged = new AllatfajokLogic(fajMockedRepo.Object);

            var eredmeny = logic.AllatEsTaplalkozas("Növényevő", seged);

            allatMockedRepo.Verify(x => x.AllatEsTaplalkozas("Növényevő", seged.GetRepo), Times.Once);
        }

        /// <summary>
        /// testing a nonCRUD method no3.
        /// </summary>
        [Test]
        public void AllatfajokDbTest()
        {
            Mock<IAllatokRepository> mockedRepo = new Mock<IAllatokRepository>(MockBehavior.Loose);
            List<Allatok> allatok = new List<Allatok>()
            {
                new Allatok() { FajID = 1, AllatNev = "cinike" },
                new Allatok() { FajID = 1, AllatNev = "cinike2" },
                new Allatok() { FajID = 2, AllatNev = "DOG" },
            };
            List<FajDbResult> expected = new List<FajDbResult>()
            {
                new FajDbResult() { FajID = 1, Darab = 2 },
                new FajDbResult() { FajID = 2, Darab = 1 },
            };
            mockedRepo.Setup(repo => repo.GetAll()).Returns(allatok.AsQueryable());
            AllatokLogic logic = new AllatokLogic(mockedRepo.Object);

            var result = logic.AllatfajosDarab("1");

            mockedRepo.Verify(repo => repo.AllatfajosDarab(1), Times.Exactly(1));
        }
    }
}
