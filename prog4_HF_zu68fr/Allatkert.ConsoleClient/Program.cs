﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Allatkert.ConsoleClient
{
    //TODO: NSwag / SwashBuckle, restAPI miatt ki kell másolni az osztályt
    //API Testing: Selenium, Postman (user friendly)
    //NuGet: Newtonsoft.JSON
    //TODO: ultiple startup projects
    public class Gondozo
    {
        public int Id { get; set; }
        public string Nev { get; set; }
        public string TelSzam { get; set; }
        public DateTime MunkaKezd { get; set; }
        public DateTime MunkaVeg { get; set; }

        public override string ToString()
        {
            return $"ID={Id}\tName={Nev}\tPhoneNumber={TelSzam}\tWorkBegin={MunkaKezd}\tWorkEnd={MunkaVeg}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();
            //meg kell várni az ASP indulását

            string url = "http://localhost:52594/api/GondozoApi/";
            //No error handling!
            //WebClient => HttpClient (post miatt, get-re elég a webclient)
            using(HttpClient client=new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Gondozo>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                //WebClient => NameValueCollection postData + byte[] responsBytes, káosz
                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Gondozo.Id), "20");
                postData.Add(nameof(Gondozo.Nev), "Macskás Erzsi");
                postData.Add(nameof(Gondozo.TelSzam), "06109998866");
                postData.Add(nameof(Gondozo.MunkaKezd), "2020.01.01 0:01:10");
                postData.Add(nameof(Gondozo.MunkaVeg), "2020.01.08 0:01:10");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData))
                    .Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: "+response);
                Console.WriteLine("ALL: "+json);
                Console.ReadLine();
                //  ADD
                //-----------------------------------------------------------------------------------------------------------------------
                int gondozoId = JsonConvert.DeserializeObject<List<Gondozo>>(json)
                    .Single(x => x.Nev == "Macskás Erzsi").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Gondozo.Id), gondozoId.ToString());
                postData.Add(nameof(Gondozo.Nev), "Macskás Bözsi");
                postData.Add(nameof(Gondozo.TelSzam), "06109998866");
                postData.Add(nameof(Gondozo.MunkaKezd), "2020.01.01 0:01:10");
                postData.Add(nameof(Gondozo.MunkaVeg), "2020.01.08 0:01:10");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData))
                    .Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
                //  MOD
                //-----------------------------------------------------------------------------------------------------------------------

                response = client.GetStringAsync(url + "del/" + gondozoId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
                //  DEL
            }
            //TODO: Client side filter vs server side filter
            //TODO: Client paging vs server side paging
        }
    }
}
