// <copyright file="Allatok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// class for db.
    /// </summary>
    [Table("Allatok")]
    public partial class Allatok
    {
        /// <summary>
        /// gets or sets.
        /// </summary>
        [Key]
        [Column(TypeName = "numeric")]
        public decimal AllatID { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [StringLength(30)]
        public string AllatNev { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal FajID { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal KifutoID { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Required]
        [StringLength(10)]
        public string Nem { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime SzuletesiIdo { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime? HalolozasiIdo { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public virtual Allatfajok Allatfajok { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        public virtual Kifutok Kifutok { get; set; }
    }
}