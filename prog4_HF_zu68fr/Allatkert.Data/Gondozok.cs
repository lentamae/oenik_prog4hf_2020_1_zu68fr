// <copyright file="Gondozok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// class for db.
    /// </summary>
    [Table("Gondozok")]
    public partial class Gondozok
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Gondozok"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "<Pending>")]
        public Gondozok()
        {
            this.Kifutok = new HashSet<Kifutok>();
        }

        /// <summary>
        /// GETS OR SETS.
        /// </summary>
        [Key]
        [Column(TypeName = "numeric")]
        public decimal GondozoID { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Required]
        [StringLength(30)]
        public string GondozoNev { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Telefonszam { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime MunkaviszKezd { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime? MunkaviszVeg { get; set; }

        /// <summary>
        /// gets or sets.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kifutok> Kifutok { get; set; }
    }
}