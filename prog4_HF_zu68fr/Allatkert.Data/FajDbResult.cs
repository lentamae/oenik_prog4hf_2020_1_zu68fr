﻿// <copyright file="FajDbResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Data
{
    /// <summary>
    /// Helper class for a nonCRUD query.
    /// </summary>
    public class FajDbResult
    {
        /// <summary>
        /// Gets or sets FajID.
        /// </summary>
        public decimal FajID { get; set; }

        /// <summary>
        /// Gets or sets Darab.
        /// </summary>
        public int Darab { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"A(z) {this.FajID} azonosítójú állatból {this.Darab} darab van";
        }
    }
}