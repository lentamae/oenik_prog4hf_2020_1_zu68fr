﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using Allatkert.Logic;

    /// <summary>
    /// main program class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// really? the main method.
        /// </summary>
        /// <param name="args">some args that we will never use.</param>
        private static void Main(string[] args)
        {
            AllatokLogic allatokLogic = new AllatokLogic();
            AllatfajokLogic allatfajokLogic = new AllatfajokLogic();
            GondozokLogic gondozokLogic = new GondozokLogic();
            KifutokLogic kifutokLogic = new KifutokLogic();
            string menupont;
            bool menu = true;

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Üdvözöllek az Óbudai állatkertben!");
                Console.WriteLine("Mit szeretnél csinálni?");
                Console.WriteLine();
                Console.WriteLine("1: Minden kiiratás");
                Console.WriteLine("2: Egy elem kiiratása");
                Console.WriteLine("3: Egy elem beszúrása");
                Console.WriteLine("4: Egy elem nevének változtatása (update)");
                Console.WriteLine("5: Egy elem törlése");
                Console.WriteLine("6: Egyéb műveletek");
                Console.WriteLine("7: Online, mogyoró előrendelés");
                Console.WriteLine("x: Kilépés");
                Console.WriteLine();
                Console.Write("Válasszon egy menüpontot: ");
                menupont = Console.ReadLine();

                switch (menupont)
                {
                    case "1":
                        while (menu)
                        {
                            Console.Clear();
                            Console.WriteLine("Miket szeretnél kiiratni?");
                            Console.WriteLine();
                            Console.WriteLine("1: Állatfajok");
                            Console.WriteLine("2: Állatok");
                            Console.WriteLine("3: Gondozók");
                            Console.WriteLine("4: Kifutók");
                            Console.WriteLine("x: Vissza");
                            Console.WriteLine();
                            Console.Write("Válasszon egy menüpontot: ");

                            menupont = Console.ReadLine();
                            switch (menupont)
                            {
                                case "1":
                                    Console.Clear();
                                    foreach (var item in allatfajokLogic.GetAllAllatfajok())
                                    {
                                        Console.WriteLine(item.FajID + " " + item.FajNev + " " + item.Taplalkozas);
                                    }

                                    Console.ReadKey();
                                    break;
                                case "2":
                                    Console.Clear();
                                    Console.WriteLine("ID".PadRight(3) + "Név".PadRight(10) +
                                        "FajID".PadRight(6) + "KifutóID".PadRight(9) +
                                        "Nem".PadRight(12) + "Születési idő".PadRight(16) +
                                        "Halálozási idő");
                                    Console.WriteLine();
                                    foreach (var item in allatokLogic.GetAllAllatok())
                                    {
                                        Console.WriteLine(item.AllatID.ToString().PadRight(3)
                                            + item.AllatNev.ToString().PadRight(10)
                                            + item.FajID.ToString().PadRight(6)
                                            + item.KifutoID.ToString().PadRight(9)
                                            + item.Nem.ToString().PadRight(12)
                                            + item.SzuletesiIdo.ToString("yyyy.MM.dd").PadRight(16)
                                            + item.HalolozasiIdo?.ToString("yyyy.MM.dd"));
                                    }

                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    foreach (var item in gondozokLogic.GetAllGondozok())
                                    {
                                        Console.WriteLine(item.GondozoID + " " + item.GondozoNev + " " +
                                            item.Telefonszam + " " + item.MunkaviszKezd.ToString("yyyy.MM.dd")
                                            + " " + item.MunkaviszVeg?.ToString("yyyy.MM.dd"));
                                    }

                                    Console.ReadKey();
                                    break;
                                case "4":
                                    Console.Clear();
                                    Console.WriteLine();
                                    foreach (var item in kifutokLogic.GetAllKifutok())
                                    {
                                        Console.WriteLine(item.KifutoID + " " + item.KifutoNev + " " +
                                            item.KifutoMeret + " " + item.GondozoID);
                                    }

                                    Console.ReadKey();
                                    break;
                                case "x":
                                    menu = false;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Nincs ilyen menüpont");
                                    Console.ReadKey();
                                    break;
                            }
                        }

                        break;

                    case "2":
                        while (menu)
                        {
                            Console.Clear();
                            Console.WriteLine("Honnan szeretnél egyet kiiratni?");
                            Console.WriteLine();
                            Console.WriteLine("1: Állatfajok");
                            Console.WriteLine("2: Állatok");
                            Console.WriteLine("3: Gondozók");
                            Console.WriteLine("4: Kifutók");
                            Console.WriteLine("x: Vissza");
                            Console.WriteLine();
                            Console.Write("Válasszon egy menüpontot: ");

                            menupont = Console.ReadLine();
                            switch (menupont)
                            {
                                case "1":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    var faj = allatfajokLogic.GetOneAllatfajok(Console.ReadLine());
                                    Console.WriteLine(faj.FajID + " " + faj.FajNev + " " + faj.Taplalkozas);

                                    break;
                                case "2":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    var allat = allatokLogic.GetOneAllatok(Console.ReadLine());
                                    Console.WriteLine("ID".PadRight(3) + "Név".PadRight(10) +
                                        "FajID".PadRight(6) + "KifutóID".PadRight(9) +
                                        "Nem".PadRight(12) + "Születési idő".PadRight(16) +
                                        "Halálozási idő");
                                    Console.WriteLine();
                                    Console.WriteLine(allat.AllatID.ToString().PadRight(3)
                                            + allat.AllatNev.ToString().PadRight(10)
                                            + allat.FajID.ToString().PadRight(6)
                                            + allat.KifutoID.ToString().PadRight(9)
                                            + allat.Nem.ToString().PadRight(12)
                                            + allat.SzuletesiIdo.ToString("yyyy.MM.dd").PadRight(16)
                                            + allat.HalolozasiIdo?.ToString("yyyy.MM.dd"));
                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    var gondozo = gondozokLogic.GetOneGondozok(Console.ReadLine());
                                    Console.WriteLine(gondozo.GondozoID + " " + gondozo.GondozoNev + " " +
                                            gondozo.Telefonszam + " " + gondozo.MunkaviszKezd.ToString("yyyy.MM.dd")
                                            + " " + gondozo.MunkaviszVeg?.ToString("yyyy.MM.dd"));

                                    break;
                                case "4":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    var kifuto = kifutokLogic.GetOneKifutok(Console.ReadLine());
                                    Console.WriteLine(kifuto.KifutoID + " " + kifuto.KifutoNev + " " +
                                            kifuto.KifutoMeret + " " + kifuto.GondozoID);

                                    break;
                                case "x":
                                    menu = false;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Nincs ilyen menüpont");
                                    Console.ReadKey();
                                    break;
                            }
                        }

                        break;

                    case "3":
                        while (menu)
                        {
                            Console.Clear();
                            Console.WriteLine("Hova szeretnél egyet beszúrni?");
                            Console.WriteLine();
                            Console.WriteLine("1: Állatfajok");
                            Console.WriteLine("2: Állatok");
                            Console.WriteLine("3: Gondozók");
                            Console.WriteLine("4: Kifutók");
                            Console.WriteLine("x: Vissza");
                            Console.WriteLine();
                            Console.Write("Válasszon egy menüpontot: ");

                            menupont = Console.ReadLine();
                            switch (menupont)
                            {
                                case "1":
                                    Console.Clear();
                                    Console.Write("ID: ");
                                    string id2 = Console.ReadLine();
                                    Console.Write("Név: ");
                                    string nev2 = Console.ReadLine();
                                    Console.Write("Táplálkozás (Növényevő/Ragadozó/Mindenevő): ");
                                    string tapl = Console.ReadLine();
                                    allatfajokLogic.InsertAllatfajok(id2, nev2, tapl);
                                    break;
                                case "2":
                                    Console.Clear();
                                    Console.Write("ID: ");
                                    string id = Console.ReadLine();
                                    Console.Write("Név: ");
                                    string nev = Console.ReadLine();
                                    Console.Write("FajID: ");
                                    string fajid = Console.ReadLine();
                                    Console.Write("KifutoID: ");
                                    string kifutoId = Console.ReadLine();
                                    Console.Write("Nem: ");
                                    string nem = Console.ReadLine();
                                    Console.Write("Születési dátum (ÉÉÉÉ.HH.NN): ");
                                    string szuldat = Console.ReadLine();
                                    Console.Write("Elhalálozási dátum (nem kötelező, ÉÉÉÉ.HH.NN): ");
                                    string haldat = Console.ReadLine();
                                    allatokLogic.InsertAllatok(id, nev, fajid, kifutoId, nem, szuldat, haldat);
                                    Console.WriteLine("Done");
                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    Console.Write("ID: ");
                                    string id3 = Console.ReadLine();
                                    Console.Write("Név: ");
                                    string nev3 = Console.ReadLine();
                                    Console.Write("Tel.szám: ");
                                    string telszam = Console.ReadLine();
                                    Console.Write("Munkakezdés ideje (ÉÉÉÉ.HH.NN): ");
                                    string munkakezd = Console.ReadLine();
                                    Console.Write("Születési dátum (nem kötelező, ÉÉÉÉ.HH.NN): ");
                                    string munkaveg = Console.ReadLine();
                                    gondozokLogic.InsertGondozok(id3, nev3, telszam, munkakezd, munkaveg);
                                    break;
                                case "4":
                                    Console.Clear();
                                    Console.Write("ID: ");
                                    string id4 = Console.ReadLine();
                                    Console.Write("Név: ");
                                    string nev4 = Console.ReadLine();
                                    Console.Write("Méret (10x10): ");
                                    string meret = Console.ReadLine();
                                    Console.Write("GondozóID: ");
                                    string gondid = Console.ReadLine();
                                    kifutokLogic.InsertKifutok(id4, nev4, meret, gondid);
                                    break;
                                case "x":
                                    menu = false;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Nincs ilyen menüpont");
                                    Console.ReadKey();
                                    break;
                            }
                        }

                        break;

                    case "4":
                        while (menu)
                        {
                            Console.Clear();
                            Console.WriteLine("Hol szeretnéd megváltoztatni a nevet?");
                            Console.WriteLine();
                            Console.WriteLine("1: Állatfajok");
                            Console.WriteLine("2: Állatok");
                            Console.WriteLine("3: Gondozók");
                            Console.WriteLine("4: Kifutók");
                            Console.WriteLine("x: Vissza");
                            Console.WriteLine();
                            Console.Write("Válasszon egy menüpontot: ");

                            menupont = Console.ReadLine();
                            switch (menupont)
                            {
                                case "1":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    string id = Console.ReadLine();
                                    Console.Write("Adja meg az új nevet: ");
                                    if (allatfajokLogic.UpdateAllatfajok(id, Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres frissítés!");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Hiba történt a frissítés során");
                                    }

                                    break;
                                case "2":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    string id2 = Console.ReadLine();
                                    Console.Write("Adja meg az új nevet: ");
                                    if (allatokLogic.UpdateAllatok(id2, Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres frissítés!");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Hiba történt a frissítés során");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    string id3 = Console.ReadLine();
                                    Console.Write("Adja meg az új nevet: ");
                                    if (gondozokLogic.UpdateGondozok(id3, Console.ReadLine(),"","","")) //"el lett rontva" (meg lett javítva)
                                    {
                                        Console.WriteLine("Sikeres frissítés!");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Hiba történt a frissítés során");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "4":
                                    Console.Clear();
                                    Console.Write("Adjon meg egy ID-t: ");
                                    string id4 = Console.ReadLine();
                                    Console.Write("Adja meg az új nevet: ");
                                    if (kifutokLogic.UpdateKifutok(id4, Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres frissítés!");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Hiba történt a frissítés során");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "x":
                                    menu = false;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Nincs ilyen menüpont");
                                    Console.ReadKey();
                                    break;
                            }
                        }

                        break;

                    case "5":
                        while (menu)
                        {
                            Console.Clear();
                            Console.WriteLine("Honnan szeretnél törölni?");
                            Console.WriteLine();
                            Console.WriteLine("1: Állatfajok");
                            Console.WriteLine("2: Állatok");
                            Console.WriteLine("3: Gondozók");
                            Console.WriteLine("4: Kifutók");
                            Console.WriteLine("x: Vissza");
                            Console.WriteLine();
                            Console.Write("Válasszon egy menüpontot: ");

                            menupont = Console.ReadLine();
                            switch (menupont)
                            {
                                case "1":
                                    Console.Clear();
                                    Console.Write("Adj meg egy id-t: ");
                                    if (allatfajokLogic.DeleteAllatfajok(Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres törlés");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "2":
                                    Console.Clear();
                                    Console.Write("Adj meg egy id-t: ");
                                    if (allatokLogic.DeleteAllatok(Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres törlés");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    Console.Write("Adj meg egy id-t: ");
                                    if (gondozokLogic.DeleteGondozok(Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres törlés");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "4":
                                    Console.Clear();
                                    Console.Write("Adj meg egy id-t: ");
                                    if (kifutokLogic.DeleteKifutok(Console.ReadLine()))
                                    {
                                        Console.WriteLine("Sikeres törlés");
                                    }

                                    Console.ReadKey();
                                    break;
                                case "x":
                                    menu = false;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Nincs ilyen menüpont");
                                    Console.ReadKey();
                                    break;
                            }
                        }

                        break;

                    case "6":
                        while (menu)
                        {
                            Console.Clear();
                            Console.WriteLine("Mit szeretnél csinálni?");
                            Console.WriteLine();
                            Console.WriteLine("1: Táplálkozás alapján állatok kiiratása");
                            Console.WriteLine("2: Melyik gondozó hány kifutóval foglalkozik?");
                            Console.WriteLine("3: Faj alapján csoportosítás");
                            Console.WriteLine("x: Vissza");
                            Console.WriteLine();
                            Console.Write("Válasszon egy menüpontot: ");

                            menupont = Console.ReadLine();
                            switch (menupont)
                            {
                                case "1":
                                    Console.Clear();
                                    Console.Write("Növényevő/Ragadozó/Mindenevő?");
                                    Console.WriteLine();
                                    foreach (var item in allatokLogic.AllatEsTaplalkozas(Console.ReadLine(), allatfajokLogic))
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Console.ReadKey();
                                    break;
                                case "2":
                                    Console.Clear();
                                    Console.WriteLine("A kért információ");
                                    Console.WriteLine();
                                    foreach (var item in kifutokLogic.GondozoKifutoCount())
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    Console.Write("Adj meg egy faj id-t ");
                                    foreach (var item in allatokLogic.AllatfajosDarab(Console.ReadLine()))
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Console.ReadKey();
                                    break;
                                case "x":
                                    menu = false;
                                    break;
                                default:
                                    Console.Clear();
                                    Console.WriteLine("Nincs ilyen menüpont");
                                    Console.ReadKey();
                                    break;
                            }
                        }

                        break;

                    case "7":
                        Console.Clear();
                        Console.WriteLine("Legacy funkció, nem működik");
                        //Console.Write("Szia!!! Milyen elefántmogyit szeretnél vásárolni? ");
                        //string mogyi = Console.ReadLine();
                        //Console.WriteLine("Raktár ellenőrzése...");
                        //XDocument xDoc = XDocument.Load("http://localhost:8080/OENIK_PROG3_2019_2_ZU68FR/szervlet?mogyi=" + mogyi);
                        //var uzenet = xDoc.Descendants("uzenet").First().Value;
                        //Console.WriteLine(uzenet);
                        //Console.Write("Raktáron:" + xDoc.Descendants("eredmeny").First().Value);
                        //Console.ReadKey();
                        break;

                    case "x":
                        Console.WriteLine("Csá");
                        Environment.Exit(0);
                        break;

                    default:
                        Console.Clear();
                        Console.WriteLine("Nincs ilyen menüpont");
                        Console.ReadKey();
                        break;
                }

                menu = true;
            }
        }
    }
}
