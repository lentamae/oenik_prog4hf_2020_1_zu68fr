﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Allatkert.Web.Models
{
    // Form modell
    public class Gondozo
    {
        [Required]  //ha valami kötelező
        [Display(Name ="Gondozó ID")]   //megjelenítendő neve
        public int Id { get; set; }

        [Required]
        [Display(Name = "Gondozó neve")]
        public string Nev { get; set; }

        [Required]
        [Display(Name = "Telefonszám")]
        public string TelSzam { get; set; }
        
        [Required]
        [Display(Name = "Munkaviszony kezdete")]
        public DateTime MunkaKezd { get; set; }
        
        [Display(Name = "Munkaviszony vége")]
        public DateTime MunkaVeg { get; set; }
    }
}