﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allatkert.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Allatkert.Data.Gondozok, Allatkert.Web.Models.Gondozo>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.GondozoID)).
                ForMember(dest => dest.Nev, map => map.MapFrom(src => src.GondozoNev)).
                ForMember(dest => dest.TelSzam, map => map.MapFrom(src => src.Telefonszam)).
                ForMember(dest => dest.MunkaKezd, map => map.MapFrom(src => src.MunkaviszKezd)).
                ForMember(dest => dest.MunkaVeg, map => map.MapFrom(src => src.MunkaviszVeg == null ? "" :
                src.MunkaviszVeg.ToString()));
                //ha üres akkor ""-t ír majd ki, különben meg a kapcsolat megfelelő neve

                //.ReverseMap()
                //src => src.valamiNev ? "" : src.kapcs.kapcsNev  (???)
                // a bonyolultabb konverziókat vissza kell fordítani, ami nem mindig triviális
            });
            return config.CreateMapper();
        }
    }
}