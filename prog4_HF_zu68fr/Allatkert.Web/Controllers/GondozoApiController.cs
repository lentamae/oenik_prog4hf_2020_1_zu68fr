﻿using Allatkert.Data;
using Allatkert.Logic;
using Allatkert.Repository;
using Allatkert.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Allatkert.Web.Controllers
{
    public class GondozoApiController : ApiController
    {
        public class ApiResult  //SINGLE POINT OF USAGE!!!
        {
            public bool OperationResult { get; set; }
        }

        IGondozokLogic logic;
        IMapper mapper;

        public GondozoApiController()
        {
            Model1 ctx = new Model1();
            GondozokRepository gondozoRepo = new GondozokRepository(ctx);
            logic = new GondozokLogic(gondozoRepo);
            mapper = MapperFactory.CreateMapper();
        }

        //GET api/GondozoApi
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Gondozo> GetAll()
        {
            var gondozok = logic.GetAllGondozok();
            return mapper.Map<IList<Data.Gondozok>, List<Models.Gondozo>>(gondozok);    
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneGondozo(int id)
        {
            bool success = logic.DeleteGondozok(id.ToString());
            return new ApiResult() { OperationResult = success };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneGondozo(Gondozo gondozo)
        {
            logic.InsertGondozok(gondozo.Id.ToString(),gondozo.Nev,gondozo.TelSzam,gondozo.MunkaKezd.ToString(),gondozo.MunkaVeg.ToString());
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneGondozo(Gondozo gondozo)
        {
            bool success= logic.UpdateGondozok(gondozo.Id.ToString(), gondozo.Nev, gondozo.TelSzam, gondozo.MunkaKezd.ToString(), gondozo.MunkaVeg.ToString());
            return new ApiResult() { OperationResult = true };  
        }
    }
}
