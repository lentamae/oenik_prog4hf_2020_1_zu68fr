﻿using Allatkert.Logic;
using Allatkert.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Allatkert.Repository;
using Allatkert.Data;

namespace Allatkert.Web.Controllers
{
    public class GondozoController : Controller
    {
        IGondozokLogic logic;
        IMapper mapper;
        GondozoViewModel vm;
        // asp.net core-ban: transient + automatic dependency injection => automata IoC

        public GondozoController()
        {
            //TODO:  connstring   progarm.appconfig => webes, web.config
            //TODO: copy db, kézzel => App_Data\
            //így most csak akkor "áll vissza", ha visszamásolom
            Model1 ctx = new Model1();
            GondozokRepository gondozoRepo = new GondozokRepository(ctx);
            logic = new GondozokLogic(gondozoRepo);
            mapper = MapperFactory.CreateMapper();
            //Better: Automatic IoC
            vm = new GondozoViewModel();
            vm.EditedGondozo = new Gondozo();
            var gondozok = logic.GetAllGondozok();
            //vm.ListOfGondozo = gondozok; ->converter
            vm.ListOfGondozo = mapper.Map<IList<Data.Gondozok>,List<Models.Gondozo>>(gondozok);

        }

        private Gondozo GetGondozoModel(int id)
        {
            Data.Gondozok oneGondozo = logic.GetOneGondozok(id.ToString());
            return mapper.Map<Data.Gondozok, Models.Gondozo>(oneGondozo);
        }

        // GET: Gondozo
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";  //csak ritkán használd, NAGYONNAGYON ritkán
            return View("GondozoIndex",vm);
        }

        // GET: Gondozo/Details/5
        public ActionResult Details(int id)
        {
            //var valami = GetGondozoModel(id);
            return View("GondozoDetails",GetGondozoModel(id));
        }

        //2x Del/Edit/Create helyett
        //1x Del, 1x Edit+GET, 1x Edit+POST

        public ActionResult Remove(int id)//remove, mert ugyanannak a névnek kell lennie mint a Gondozo.cshtml-ben
        {
            TempData["editResult"] = "Del failed";
            if (logic.DeleteGondozok(id.ToString()))
            {
                TempData["editResult"] = "Del OK";
            }
            return RedirectToAction(nameof(Index));   //átdob index metódusra
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedGondozo = GetGondozoModel(id);
            return View("GondozoIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Gondozo gondozo, string editAction)
        {
            if (ModelState.IsValid && gondozo!=null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction=="AddNew")
                {
                    logic.InsertGondozok(gondozo.Id.ToString(), gondozo.Nev, gondozo.TelSzam, 
                        gondozo.MunkaKezd.ToString(), gondozo.MunkaVeg.ToString());
                }
                else
                {
                    bool success = logic.UpdateGondozok(gondozo.Id.ToString(),gondozo.Nev,gondozo.TelSzam,
                        gondozo.MunkaKezd.ToString(),gondozo.MunkaVeg.ToString());
                    if (!success)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction(nameof(Index));
                
            }
            else   //felvett adat hibás
            {
                ViewData["editAction"] = "Edit";
                vm.EditedGondozo = gondozo;
                return View("GondozoIndex", vm);
            }
        }
    }
}
