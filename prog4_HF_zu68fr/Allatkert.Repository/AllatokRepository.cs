﻿// <copyright file="AllatokRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Allatkert.Data;

    /// <summary>
    /// repo class file for allatok.
    /// </summary>
    public class AllatokRepository : Repository<Allatok>, IAllatokRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AllatokRepository"/> class.
        /// </summary>
        /// <param name="ctx">base.</param>
        public AllatokRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// updates one item in db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateAllatok(int id, string ujnev)
        {
            var update = this.GetAll().Single(x => x.AllatID == id);
            if (update != null)
            {
                update.AllatNev = ujnev;
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// dels one item from db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public override bool Delete(int id)
        {
            Allatok del = this.GetOne(id);

            if (del != null)
            {
                this.ctx.Set<Allatok>().Remove(del);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// gets one item from db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one item.</returns>
        public override Allatok GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.AllatID == id);
        }

        /// <summary>
        /// inserts one item into db.
        /// </summary>
        /// <param name="id">is.</param>
        /// <param name="nev">newname.</param>
        /// <param name="fajID">speciesID.</param>
        /// <param name="kifutoID">cageID.</param>
        /// <param name="nem">sex.</param>
        /// <param name="szulDate">birthdate.</param>
        /// <returns>success.</returns>
        public bool InsertAllatok(int id, string nev, int fajID, int kifutoID, string nem, DateTime szulDate)
        {
            Allatok insert = new Allatok()
                {
                    AllatID = id,
                    AllatNev = nev,
                    FajID = fajID,
                    KifutoID = kifutoID,
                    Nem = nem,
                    SzuletesiIdo = szulDate,
                };

            this.ctx.Set<Allatok>().Add(insert);
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// inserts one item into db.
        /// </summary>
        /// <param name="id">is.</param>
        /// <param name="nev">newname.</param>
        /// <param name="fajID">speciesID.</param>
        /// <param name="kifutoID">cageID.</param>
        /// <param name="nem">sex.</param>
        /// <param name="szulDate">birthdate.</param>
        /// /// <param name="halalDate">deathdate(not necessary).</param>
        /// <returns>success.</returns>
        public bool InsertAllatok(int id, string nev, int fajID, int kifutoID, string nem, DateTime szulDate, DateTime halalDate)
        {
            Allatok insert = new Allatok()
            {
                AllatID = id,
                AllatNev = nev,
                FajID = fajID,
                KifutoID = kifutoID,
                Nem = nem,
                SzuletesiIdo = szulDate,
                HalolozasiIdo = halalDate,
            };

            this.ctx.Set<Allatok>().Add(insert);
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// noncrud method.
        /// </summary>
        /// <param name="taplalkozas">foodtype.</param>
        /// <param name="fajRepo">fajrepo.</param>
        /// <returns>custom class collection.</returns>
        public IQueryable<AllatEsFajResult> AllatEsTaplalkozas(string taplalkozas, IAllatfajokRepository fajRepo)
        {
            var aleredmeny = (from allatfajok in fajRepo.GetAll()
                              where allatfajok.Taplalkozas == taplalkozas
                              select allatfajok.FajID).ToArray();
            var eredmeny = from allatok in this.GetAll()
                           join allatfajok in aleredmeny
                           on allatok.FajID equals allatfajok
                           select new AllatEsFajResult()
                           {
                               AllatNeve = allatok.AllatNev,
                           };
            return eredmeny;
        }

        /// <summary>
        /// custom noncrud method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>custom class coll.</returns>
        public IQueryable<FajDbResult> AllatfajosDarab(int id)
        {
            var eredmeny = from allatok in this.GetAll()
                           group allatok by allatok.FajID into csoportok
                           where csoportok.Key == id
                           select new FajDbResult()
                           {
                               FajID = csoportok.Key,
                               Darab = csoportok.Select(x => x.FajID).Count(),
                           };
            return eredmeny;
        }
    }
}