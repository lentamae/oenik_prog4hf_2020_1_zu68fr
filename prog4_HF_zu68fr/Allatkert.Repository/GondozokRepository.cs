﻿// <copyright file="GondozokRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Allatkert.Data;

    /// <summary>
    /// class for repo.
    /// </summary>
    public class GondozokRepository : Repository<Gondozok>, IGondozokRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GondozokRepository"/> class.
        /// </summary>
        /// <param name="ctx">base.</param>
        public GondozokRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// dels one from db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public override bool Delete(int id)
        {
            Gondozok del = this.GetOne(id);

            if (del != null)
            {
                this.ctx.Set<Gondozok>().Remove(del);
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// gets one item from db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public override Gondozok GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.GondozoID == id);
        }

        /// <summary>
        /// inserts one item into db.
        /// </summary>
        /// <param name="id">is.</param>
        /// <param name="gondozoNev">name.</param>
        /// <param name="telefonszam">phone num.</param>
        /// <param name="munkaKezd">work begin.</param>
        /// <param name="munkaVeg">work end.</param>
        /// <returns>success.</returns>
        public bool InsertGondozok(int id, string gondozoNev, string telefonszam, DateTime munkaKezd, DateTime munkaVeg)
        {
            var maxID = this.GetAll().Max(x => x.GondozoID);
            Gondozok insert = new Gondozok()
            {
                GondozoID = maxID + 1,
                GondozoNev = gondozoNev,
                Telefonszam = telefonszam,
                MunkaviszKezd = munkaKezd,
                MunkaviszVeg = munkaVeg,
            };

            this.ctx.Set<Gondozok>().Add(insert);
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// inserts one item into db.
        /// </summary>
        /// <param name="id">is.</param>
        /// <param name="gondozoNev">name.</param>
        /// <param name="telefonszam">phone num.</param>
        /// <param name="munkaKezd">work begin.</param>
        /// <returns>success.</returns>
        public bool InsertGondozok(int id, string gondozoNev, string telefonszam, DateTime munkaKezd)
        {
            Gondozok insert = new Gondozok()
            {
                GondozoID = id,
                GondozoNev = gondozoNev,
                Telefonszam = telefonszam,
                MunkaviszKezd = munkaKezd,
            };

            this.ctx.Set<Gondozok>().Add(insert);
            this.ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// update one item on db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateGondozok(int id, string ujNev, string telefonszam, DateTime munkaKezd, DateTime munkaVeg)
        {
            var update = this.GetAll().SingleOrDefault(x => x.GondozoID == id);
            if (update != null)
            {
                update.GondozoNev = ujNev;
                update.GondozoID = id;
                update.Telefonszam = telefonszam;
                update.MunkaviszKezd = munkaKezd;
                update.MunkaviszVeg = munkaVeg;
                this.ctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
