﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Allatkert.Data;

    /// <summary>
    /// Generic repo műveletek.
    /// </summary>
    /// <typeparam name="T">any class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// gets one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one item.</returns>
        T GetOne(int id);

        /// <summary>
        /// gets all.
        /// </summary>
        /// <returns>all item.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// dels one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        bool Delete(int id);
    }

    /// <summary>
    /// custom interface for repo.
    /// </summary>
    public interface IAllatfajokRepository : IRepository<Allatfajok>
    {
        /// <summary>
        /// insert one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="fajNev">name.</param>
        /// <param name="taplalkozas">foodtype.</param>
        /// <returns>success.</returns>
        bool InsertAllatfajok(int id, string fajNev, string taplalkozas);

        /// <summary>
        /// update one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        bool UpdateAllatfajok(int id, string ujnev);
    }

    /// <summary>
    /// custom interface for repo.
    /// </summary>
    public interface IAllatokRepository : IRepository<Allatok>
    {
        /// <summary>
        /// get custom results.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>custom class res.</returns>
        IQueryable<FajDbResult> AllatfajosDarab(int id);

        /// <summary>
        /// get custom results.
        /// </summary>
        /// <param name="taplalkozas">foodtype.</param>
        /// <param name="repo">repo.</param>
        /// <returns>custom class res.</returns>
        IQueryable<AllatEsFajResult> AllatEsTaplalkozas(string taplalkozas, IAllatfajokRepository repo);

        /// <summary>
        /// insert one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="fajID">speciesID.</param>
        /// <param name="kifutoID">cageID.</param>
        /// <param name="nem">sex.</param>
        /// <param name="szulDate">birthdate.</param>
        /// <param name="halalDate">deathdate.</param>
        /// <returns>success.</returns>
        bool InsertAllatok(int id, string nev, int fajID, int kifutoID, string nem, DateTime szulDate, DateTime halalDate);

        /// <summary>
        /// insert one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="fajID">speciesID.</param>
        /// <param name="kifutoID">cageID.</param>
        /// <param name="nem">sex.</param>
        /// <param name="szulDate">birthdate.</param>
        /// <returns>success.</returns>
        bool InsertAllatok(int id, string nev, int fajID, int kifutoID, string nem, DateTime szulDate);

        /// <summary>
        /// upadtes one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        bool UpdateAllatok(int id, string ujnev);
    }

    /// <summary>
    /// custom interface for repo.
    /// </summary>
    public interface IGondozokRepository : IRepository<Gondozok>
    {
        /// <summary>
        /// insert one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="gondozoNev">name.</param>
        /// <param name="telefonszam">phonenum.</param>
        /// <param name="munkaKezd">workbegin.</param>
        /// <returns>success.</returns>
        bool InsertGondozok(int id, string gondozoNev, string telefonszam, DateTime munkaKezd);

        /// <summary>
        /// insert one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="gondozoNev">name.</param>
        /// <param name="telefonszam">phonenum.</param>
        /// <param name="munkaKezd">workbegin.</param>
        /// <param name="munkaVeg">workend.</param>
        /// <returns>success.</returns>
        bool InsertGondozok(int id, string gondozoNev, string telefonszam, DateTime munkaKezd, DateTime munkaVeg);

        /// <summary>
        /// update one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        bool UpdateGondozok(int id, string gondozoNev, string telefonszam, DateTime munkaKezd, DateTime munkaVeg);
    }

    /// <summary>
    /// custom interface for repo.
    /// </summary>
    public interface IKifutokRepository : IRepository<Kifutok>
    {
        /// <summary>
        /// insert one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="kifutoNev">name.</param>
        /// <param name="meret">size.</param>
        /// <param name="gondozoId">caretakerID.</param>
        /// <returns>success.</returns>
        bool InsertKifutok(int id, string kifutoNev, string meret, int gondozoId);

        /// <summary>
        /// update one.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        bool UpdateKifutok(int id, string ujnev);

        /// <summary>
        /// custom nonCRUD query.
        /// </summary>
        /// <returns>custom class result collection.</returns>
        IQueryable<GondozoKifutoCountResult> GondozoKifutoCount();
    }
}
