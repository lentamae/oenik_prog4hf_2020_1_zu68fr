﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Allatkert.Data;

    /// <summary>
    /// generic repo for generic stuff.
    /// </summary>
    /// <typeparam name="T">any repo.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// dbcontext for db stuff.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="ctx">base.</param>
        public Repository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// generic delete.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public abstract bool Delete(int id);

        /// <summary>
        /// generic getone.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one item.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// generic getall.
        /// </summary>
        /// <returns>all items.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }
    }
}
