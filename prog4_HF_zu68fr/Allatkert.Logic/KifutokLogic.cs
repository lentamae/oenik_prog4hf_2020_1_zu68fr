﻿// <copyright file="KifutokLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Allatkert.Data;
    using Allatkert.Repository;

    /// <summary>
    /// Type specific class.
    /// </summary>
    public class KifutokLogic : IKifutokLogic
    {
        private readonly IKifutokRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="KifutokLogic"/> class.
        /// </summary>
        public KifutokLogic()
        {
            this.repo = new KifutokRepoisitory(new Model1());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KifutokLogic"/> class.
        /// </summary>
        /// <param name="repo">moq repo.</param>
        public KifutokLogic(IKifutokRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// gets repo.
        /// </summary>
        public IKifutokRepository GetRepo
        {
            get { return this.repo; }
        }

        /// <summary>
        /// del one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public bool DeleteKifutok(string id)
        {
            return this.repo.Delete(Convert.ToInt32(id));
        }

        /// <summary>
        /// get all kifuto.
        /// </summary>
        /// <returns>retruns all item.</returns>
        public IList<Kifutok> GetAllKifutok()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// ret one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one kifuto.</returns>
        public Kifutok GetOneKifutok(string id)
        {
            return this.repo.GetOne(Convert.ToInt32(id));
        }

        /// <summary>
        /// inserts one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="kifutoMeret">size.</param>
        /// <param name="gondozoId">gondozoID.</param>
        public void InsertKifutok(string id, string nev, string kifutoMeret, string gondozoId)
        {
            this.repo.InsertKifutok(Convert.ToInt32(id), nev, kifutoMeret, Convert.ToInt32(gondozoId));
        }

        /// <summary>
        /// updates one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateKifutok(string id, string ujnev)
        {
            return this.repo.UpdateKifutok(Convert.ToInt32(id), ujnev);
        }

        /// <summary>
        /// gets a custom, nonCDUD query result.
        /// </summary>
        /// <returns>custom class results.</returns>
        public IList<GondozoKifutoCountResult> GondozoKifutoCount()
        {
            return this.repo.GondozoKifutoCount().ToList();
        }
    }
}