﻿// <copyright file="AllatfajokLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Allatkert.Data;
    using Allatkert.Repository;

    /// <summary>
    /// logic class.
    /// </summary>
    public class AllatfajokLogic : IAllatfajokLogic
    {
        private readonly IAllatfajokRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AllatfajokLogic"/> class.
        /// </summary>
        public AllatfajokLogic()
        {
            this.repo = new AllatfajokRepository(new Model1());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AllatfajokLogic"/> class.
        /// </summary>
        /// <param name="repo">irepo for moq.</param>
        public AllatfajokLogic(IAllatfajokRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// gets repo.
        /// </summary>
        public IAllatfajokRepository GetRepo
        {
            get { return this.repo; }
        }

        /// <summary>
        /// del one.
        /// </summary>
        /// <param name="id">del id.</param>
        /// <returns>del success.</returns>
        public bool DeleteAllatfajok(string id)
        {
            return this.repo.Delete(Convert.ToInt32(id));
        }

        /// <summary>
        /// get all.
        /// </summary>
        /// <returns>all items.</returns>
        public IList<Allatfajok> GetAllAllatfajok()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// get one.
        /// </summary>
        /// <param name="id">item id.</param>
        /// <returns>one item.</returns>
        public Allatfajok GetOneAllatfajok(string id)
        {
            return this.repo.GetOne(Convert.ToInt32(id));
        }

        /// <summary>
        /// insert one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="taplalkozas">taplaling.</param>
        public void InsertAllatfajok(string id, string nev, string taplalkozas)
        {
            this.repo.InsertAllatfajok(Convert.ToInt32(id), nev, taplalkozas);
        }

        /// <summary>
        /// update one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">new name.</param>
        /// <returns>success.</returns>
        public bool UpdateAllatfajok(string id, string ujnev)
        {
            return this.repo.UpdateAllatfajok(Convert.ToInt32(id), ujnev);
        }
    }
}