﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Logic
{
    using System.Collections.Generic;
    using Allatkert.Data;

    /// <summary>
    /// Type specific interface.
    /// </summary>
    public interface IAllatokLogic
    {
        /// <summary>
        /// nonCRUD method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>gourped results.</returns>
        IList<FajDbResult> AllatfajosDarab(string id);

        /// <summary>
        /// returns one allat.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one allat.</returns>
        Allatok GetOneAllatok(string id);

        /// <summary>
        /// ret all allat.
        /// </summary>
        /// <returns>all allat.</returns>
        IList<Allatok> GetAllAllatok();

        /// <summary>
        /// dels one allat.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        bool DeleteAllatok(string id);

        /// <summary>
        /// inserts one allat.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="fajID">speciesID.</param>
        /// <param name="kifutoID">cageID.</param>
        /// <param name="nem">sex.</param>
        /// <param name="szulid">birthdate.</param>
        /// <param name="halido">deathdate (not necessary).</param>
        void InsertAllatok(string id, string nev, string fajID, string kifutoID, string nem, string szulid, string halido);

        /// <summary>
        /// updates one allat.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        bool UpdateAllatok(string id, string ujnev);

        /// <summary>
        /// nonCRUD method.
        /// </summary>
        /// <param name="taplalkozas">taplaling.</param>
        /// <param name="fajL">fajLogic.</param>
        /// <returns>custom class results.</returns>
        IList<AllatEsFajResult> AllatEsTaplalkozas(string taplalkozas, AllatfajokLogic fajL);
    }

    /// <summary>
    /// Type specific interface.
    /// </summary>
    public interface IAllatfajokLogic
    {
        /// <summary>
        /// returns one faj.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one faj.</returns>
        Allatfajok GetOneAllatfajok(string id);

        /// <summary>
        /// ret all faj.
        /// </summary>
        /// <returns>all faj.</returns>
        IList<Allatfajok> GetAllAllatfajok();

        /// <summary>
        /// dels one faj.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        bool DeleteAllatfajok(string id);

        /// <summary>
        /// inserts one faj.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="taplalkozas">taplaling.</param>
        void InsertAllatfajok(string id, string nev, string taplalkozas);

        /// <summary>
        /// updates one faj.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">new name.</param>
        /// <returns>success.</returns>
        bool UpdateAllatfajok(string id, string ujnev);
    }

    /// <summary>
    /// Type specific interface.
    /// </summary>
    public interface IKifutokLogic
    {
        /// <summary>
        /// ret one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one kifuto.</returns>
        Kifutok GetOneKifutok(string id);

        /// <summary>
        /// ret all kifuto.
        /// </summary>
        /// <returns>all kifuto.</returns>
        IList<Kifutok> GetAllKifutok();

        /// <summary>
        /// del one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        bool DeleteKifutok(string id);

        /// <summary>
        /// ins one kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="kifutoMeret">size.</param>
        /// <param name="gondozoId">gondozoID.</param>
        void InsertKifutok(string id, string nev, string kifutoMeret, string gondozoId);

        /// <summary>
        /// update oine kifuto.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">new name.</param>
        /// <returns>success.</returns>
        bool UpdateKifutok(string id, string ujnev);

        /// <summary>
        /// returns nonCRUD reuslts.
        /// </summary>
        /// <returns>custom class reults.</returns>
        IList<GondozoKifutoCountResult> GondozoKifutoCount();
    }

    /// <summary>
    /// Type specific interface.
    /// </summary>
    public interface IGondozokLogic
    {
        /// <summary>
        /// ret one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one gondozo.</returns>
        Gondozok GetOneGondozok(string id);

        /// <summary>
        /// ret all gondozo.
        /// </summary>
        /// <returns>all gondozo.</returns>
        IList<Gondozok> GetAllGondozok();

        /// <summary>
        /// del one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        bool DeleteGondozok(string id);

        /// <summary>
        /// inserts one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="telefonszam">phone num.</param>
        /// <param name="munkaKezd">work begin.</param>
        /// <param name="munkaVeg">work end.</param>
        void InsertGondozok(string id, string nev, string telefonszam, string munkaKezd, string munkaVeg);

        /// <summary>
        /// updates one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        bool UpdateGondozok(string id, string ujnev, string telefonszam, string munkaKezd, string munkaVeg);
    }
}