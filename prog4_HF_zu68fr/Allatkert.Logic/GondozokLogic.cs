﻿// <copyright file="GondozokLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Allatkert.Data;
    using Allatkert.Repository;

    /// <summary>
    /// logic class for repo.
    /// </summary>
    public class GondozokLogic : IGondozokLogic
    {
        private readonly IGondozokRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GondozokLogic"/> class.
        /// </summary>
        public GondozokLogic()
        {
            this.repo = new GondozokRepository(new Model1());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GondozokLogic"/> class.
        /// </summary>
        /// <param name="repo">repo for moq.</param>
        public GondozokLogic(IGondozokRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// gets repo.
        /// </summary>
        public IGondozokRepository GetRepo
        {
            get { return this.repo; }
        }

        /// <summary>
        /// delete one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public bool DeleteGondozok(string id)
        {
            return this.repo.Delete(Convert.ToInt32(id));
        }

        /// <summary>
        /// returns all gondozo.
        /// </summary>
        /// <returns>all item.</returns>
        public IList<Gondozok> GetAllGondozok()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// returns one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one gondozo.</returns>
        public Gondozok GetOneGondozok(string id)
        {
            return this.repo.GetOne(Convert.ToInt32(id));
        }

        /// <summary>
        /// inserts one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="telefonszam">phone num.</param>
        /// <param name="munkaKezd">work begin.</param>
        /// <param name="munkaVeg">work end (not necessary).</param>
        public void InsertGondozok(string id, string nev, string telefonszam, string munkaKezd, string munkaVeg)
        {

            if (munkaVeg == string.Empty)
            {
                this.repo.InsertGondozok(Convert.ToInt32(id), nev, telefonszam, Convert.ToDateTime(munkaKezd));
            }
            else
            {
                this.repo.InsertGondozok(Convert.ToInt32(id), nev, telefonszam, Convert.ToDateTime(munkaKezd), Convert.ToDateTime(munkaVeg));
            }
        }

        /// <summary>
        /// updates one gondozo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateGondozok(string id, string nev, string telefonszam, string munkaKezd, string munkaVeg)
        {
            return this.repo.UpdateGondozok(Convert.ToInt32(id), nev, telefonszam, Convert.ToDateTime(munkaKezd), Convert.ToDateTime(munkaVeg));
        }
    }
}
