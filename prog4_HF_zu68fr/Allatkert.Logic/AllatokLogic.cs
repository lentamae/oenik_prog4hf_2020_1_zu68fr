﻿// <copyright file="AllatokLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Allatkert.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Allatkert.Data;
    using Allatkert.Repository;

    /// <summary>
    /// logic for repo.
    /// </summary>
    public class AllatokLogic : IAllatokLogic
    {
        private readonly IAllatokRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AllatokLogic"/> class.
        /// </summary>
        public AllatokLogic()
        {
            this.repo = new AllatokRepository(new Model1());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AllatokLogic"/> class.
        /// </summary>
        /// <param name="repo">repo for moq.</param>
        public AllatokLogic(IAllatokRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// gets repo.
        /// </summary>
        public IAllatokRepository GetRepo
        {
            get { return this.repo; }
        }

        /// <summary>
        /// nonCRUD method.
        /// </summary>
        /// <param name="taplalkozas">taplaling for search.</param>
        /// <param name="fajL">logic for multi-table query.</param>
        /// <returns>results for the query.</returns>
        public IList<AllatEsFajResult> AllatEsTaplalkozas(string taplalkozas, AllatfajokLogic fajL)
        {
            var aleredmeny = (from allatfajok in fajL.GetRepo.GetAll()
                             where allatfajok.Taplalkozas == taplalkozas
                             select allatfajok.FajID).ToArray();

            return this.repo.AllatEsTaplalkozas(taplalkozas, fajL.GetRepo).ToList();
        }

        /// <summary>
        /// del item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>success.</returns>
        public bool DeleteAllatok(string id)
        {
            return this.repo.Delete(Convert.ToInt32(id));
        }

        /// <summary>
        /// get all item.
        /// </summary>
        /// <returns>all item.</returns>
        public IList<Allatok> GetAllAllatok()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// returns one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one item.</returns>
        public Allatok GetOneAllatok(string id)
        {
            return this.repo.GetOne(Convert.ToInt32(id));
        }

        /// <summary>
        /// inserts one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="nev">name.</param>
        /// <param name="fajID">speciesID.</param>
        /// <param name="kifutoID">cageID.</param>
        /// <param name="nem">sex.</param>
        /// <param name="szulido">birthdate.</param>
        /// <param name="halido">deathdate(not necessary).</param>
        public void InsertAllatok(string id, string nev, string fajID, string kifutoID, string nem, string szulido, string halido)
        {
            if (halido == string.Empty)
            {
                this.repo.InsertAllatok(Convert.ToInt32(id), nev, Convert.ToInt32(fajID), Convert.ToInt32(kifutoID), nem, Convert.ToDateTime(szulido));
            }
            else
            {
                this.repo.InsertAllatok(Convert.ToInt32(id), nev, Convert.ToInt32(fajID), Convert.ToInt32(kifutoID), nem, Convert.ToDateTime(szulido), Convert.ToDateTime(halido));
            }
        }

        /// <summary>
        /// update one item.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="ujnev">newname.</param>
        /// <returns>success.</returns>
        public bool UpdateAllatok(string id, string ujnev)
        {
            return this.repo.UpdateAllatok(Convert.ToInt32(id), ujnev);
        }

        /// <summary>
        /// nonCRUD query.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>grouped results.</returns>
        public IList<FajDbResult> AllatfajosDarab(string id)
        {
            return this.repo.AllatfajosDarab(Convert.ToInt32(id)).ToList();
        }
    }
}
