﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Allatkert.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:52594/api/GondozoApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "GondozoResult");
        }

        public List<GondozoVM> ApiGetGondozok()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<GondozoVM>>(json);
            //NEVER!!! messenger miatt, mert buta, Végtelen ciklus    SendMessage(true);
            return list;
        }

        public void ApiDelGondozo(GondozoVM gondozo)
        {
            bool success = false;
            if (gondozo!=null)
            {
                string json = client.GetStringAsync(url + "del/" + gondozo.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];

            }
            SendMessage(success);
        }

        bool ApiEditGondozo(GondozoVM gondozo,bool isEditing)
        {
            if (gondozo== null) 
            {
                return false;
            }
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(GondozoVM.Id), gondozo.Id.ToString());
            }
            postData.Add(nameof(GondozoVM.Nev), gondozo.Nev);
            postData.Add(nameof(GondozoVM.TelSzam), gondozo.TelSzam);
            postData.Add(nameof(GondozoVM.MunkaKezd), gondozo.MunkaKezd.ToString());
            postData.Add(nameof(GondozoVM.MunkaVeg), gondozo.MunkaVeg.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData))
                .Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditGondozo(GondozoVM gondozo, Func<GondozoVM,bool> editor)//func jó lesz ablakfeldobásra
        {
            GondozoVM clone = new GondozoVM();
            if (gondozo!=null)
            {
                clone.CopyFrom(gondozo);
            }

            bool? success = editor?.Invoke(clone);  //lehet a bool null is -> 'bool?'
            if (success==true)
            {
                if (gondozo!=null)
                {
                    success=ApiEditGondozo(clone, true);
                }
                else
                {
                    success = ApiEditGondozo(clone, false);
                }
            }
            SendMessage(success == true);

        }
    }
}
