﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Allatkert.Wpf
{
    //NuGet: Newtonsoft.JSON, mvvmlightlibs
    //Startup: web+wpf
    class GondozoVM: ObservableObject
    {
        private int id;
        private string name;
        private string telSzam;
        private DateTime munkaKezd;
        private DateTime munkaVeg;

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }
        public string Nev
        {
            get { return name; }
            set { Set(ref name, value); }
        }
        public string TelSzam
        {
            get { return telSzam; }
            set { Set(ref telSzam, value); }
        }
        public DateTime MunkaKezd
        {
            get { return munkaKezd; }
            set { Set(ref munkaKezd, value); }
        }
        public DateTime MunkaVeg
        {
            get { return munkaVeg; }
            set { Set(ref munkaVeg, value); }
        }

        public void CopyFrom(GondozoVM other)
        {
            if (other!=null)
            {
                this.Id = other.Id;
                this.Nev = other.Nev;
                this.TelSzam = other.TelSzam;
                this.MunkaKezd = other.MunkaKezd;
                this.MunkaVeg = other.MunkaVeg;
            }
        }
    }
}
