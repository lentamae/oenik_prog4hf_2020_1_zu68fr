﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Allatkert.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private GondozoVM selectedGondozo;
        private ObservableCollection<GondozoVM> allGondozok;

        public ObservableCollection<GondozoVM> AllGondozok
        {
            get { return allGondozok; }
            set { Set(ref allGondozok, value); }
        }

        public GondozoVM SelectedGondozo
        {
            get { return selectedGondozo; }
            set { Set(ref selectedGondozo, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<GondozoVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();

            DelCmd = new RelayCommand(() => logic.ApiDelGondozo(selectedGondozo));
            AddCmd = new RelayCommand(() => logic.EditGondozo(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditGondozo(selectedGondozo, EditorFunc));
            LoadCmd = new RelayCommand(() => AllGondozok = new ObservableCollection<GondozoVM>(logic.ApiGetGondozok()));
        }


    }
}
